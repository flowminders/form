<div class="form-group @if($errors->has($field->getEscapedName())) has-error @endif">
    <label for="{{ $field->getName() }}" class="control-label">{{ $field->getLabel() }}</label> <br>
    @foreach($field->radios() as $radio)
        {!! $radio->setView('flowcontrol/form::radio_inline')->render() !!}
    @endforeach
    @include('flowcontrol/form::_error')
</div>