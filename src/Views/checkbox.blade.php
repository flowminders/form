<div class=" @if($errors->has($field->getEscapedName())) has-error @endif ">
    <div class="checkbox">
        <label>
            <input type="checkbox" {!! $field->attributes() !!} id="{{ $field->getName() }}" name="{{ $field->getName() }}" value="{{ $field->getValue() }}">
            <i class="input-helper"></i>
            {{ $field->getLabel() }}
        </label>
        @include('flowcontrol/form::_error')
    </div>
</div>