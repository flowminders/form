@extends('flowcontrol::layout.master')

@section('content')

    @php $model = $form->getModel() @endphp
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ $title or '' }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! form($form) !!}
                </div>
            </div><!-- /.box -->
        </div>
    </div>
@stop

@section('scripts')

    <script src="{{asset('assets/admin/js/main.js')}}"></script>

@endsection
