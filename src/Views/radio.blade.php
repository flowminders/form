<div class=" @if($errors->has($field->getEscapedName())) has-error @endif ">
    <div class="radio">
        <label>
            <input type="radio" {!! $field->attributes() !!} id="{{ $field->getName() }}" name="{{ $field->getName() }}" value="{{ $field->getValue() }}">
            <i class="input-helper"></i>
            {{ $field->getLabel() }}
        </label>
        @include('flowcontrol/form::_error')
    </div>
</div>