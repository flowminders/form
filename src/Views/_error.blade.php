@if($errors->has($field->getName()))
    <span class="help-block">{{ $errors->first($field->getName()) }}</span>
@elseif($errors->has($field->getEscapedName()))
    <span class="help-block">{{ $errors->first($field->getEscapedName()) }}</span>
@endif