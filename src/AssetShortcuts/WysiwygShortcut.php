<?php

namespace FlowControl\Form\AssetShortcuts;

use FlowControl\Assets\Contracts\Shortcut;
use Asset;

/**
 * @codeCoverageIgnore
 */
class WysiwygShortcut implements Shortcut
{
    public function execute()
    {
        Asset::addJs(
            '/vendor/flowcontrol/form/tinymce/tinymce.min.js',
            10
        );

        Asset::addJs(
            '/vendor/flowcontrol/form/form.js',
            0
        );
    }
}
