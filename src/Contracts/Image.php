<?php

namespace FlowControl\Form\Contracts;

interface Image
{
    public function setSrc($src);
    public function getSrc();
}