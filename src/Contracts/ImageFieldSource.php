<?php

namespace FlowControl\Form\Contracts;

interface ImageFieldSource
{
    public function getImagePath($field);
}