<?php

namespace FlowControl\Form\Contracts;

interface ValidatesWhenSubmitted
{
    /**
     * Validate a form after submission.
     *
     * @return void
     */
    public function validate();
}
