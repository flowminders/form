<?php

namespace FlowControl\Form\Field;

class Option extends AbstractType
{
    public function render(array $attributes = [], array $viewData = [])
    {
        $this->options = array_merge($this->options, $attributes);
        $this->options['value'] = $this->getName();
        $this->value = $this->getName();

        return parent::render($attributes, $viewData);
    }
}
