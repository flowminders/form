<?php

namespace FlowControl\Form\Field;

use Asset;

class Wysiwyg extends Textarea
{
    public function __construct($name, $label, $options = null)
    {
        Asset::wysiwyg();
        parent::__construct($name, $label, $options);

        $this->setView('flowcontrol/form::textarea');
        $this->options['class'] = array_get($this->getOptions(), 'class').' flowcontrol-wysiwyg';
    }
}
