<?php

namespace FlowControl\Form\Field;

class CheckboxGroup extends AbstractType
{
    protected $checkboxes = [];

    public function checkboxes()
    {
        return $this->checkboxes;
    }

    public function checkbox($label, $value = null, array $attributes = [])
    {
        $value = is_null($value) ? $this->getValue() : $value;
        $this->checkboxes[] = (new Checkbox($this->getName(), $label, $attributes))->setValue($value);
        return $this;
    }

}