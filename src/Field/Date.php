<?php

namespace FlowControl\Form\Field;

class Date extends Text
{
    public function __construct($name, $label, $options = null)
    {
        parent::__construct($name, $label, $options);

        $this->setView('flowcontrol/form::text');
        $this->options['type'] = 'date';
    }
}