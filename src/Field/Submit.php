<?php

namespace FlowControl\Form\Field;

class Submit extends AbstractType
{
    public function __construct($name, $label, $options = null)
    {
        parent::__construct($name, $label, $options);

        $this->setView('flowcontrol/form::submit');
        $this->options['type'] = 'submit';
        $this->setValue($label);
    }
}
