<?php

namespace FlowControl\Form\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @codeCoverageIgnore
 */
class MakeFormCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'flowcontrol:form';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a form class.';

    protected $type = 'Form class';

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function fire()
    {
        $inputName = $this->getNameInput();
        $noFormName = str_replace('Form', '', $inputName);
        $singular = str_singular($noFormName);
        $singularForm = $singular . 'Form';

        $name = $this->parseName($singularForm);

        $path = $this->getPath($name);

        if ($this->alreadyExists($singularForm)) {
            $this->error($this->type.' already exists!');

            return false;
        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($name));

        $this->info($this->type.' created successfully.');


        $snake = snake_case($singular, '-');
        $name = str_plural($snake);

        $from = __DIR__ . '/stubs/form.blade.stub';

        $viewPath = config('flowcontrol.viewPath');

        if(strlen($viewPath) > 0) {
            $viewPath .= '/';
        }

        $targetPath = resource_path("views/{$viewPath}{$name}/");
        $fileName = 'form.blade.php';

        if( $this->files->exists($targetPath . $fileName) )
        {
            $this->error("File views/{$viewPath}{$name}/{$fileName} already exists!");
            return;
        }

        if( !$this->files->isDirectory($targetPath) )
        {
            $this->files->makeDirectory($targetPath, 0755, true);
        }

        if( $this->files->copy($from, $targetPath . $fileName) )
        {
            $this->info("Created views/{$viewPath}{$name}/{$fileName}");
            return;
        }

        $this->error("Could not create views/{$viewPath}{$name}/{$fileName}");
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/form.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\Forms';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the form class.'],
        ];
    }
}
