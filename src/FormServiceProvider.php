<?php

namespace FlowControl\Form;

use FlowControl\Form\AssetShortcuts\WysiwygShortcut;
use FlowControl\Form\Commands\MakeFormCommand;
use FlowControl\Form\Contracts\ValidatesWhenSubmitted;
use Asset;
use Illuminate\Support\ServiceProvider;

/**
 * Class FormServiceProvider.
 *
 * @codeCoverageIgnore
 */
class FormServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->afterResolving(function (ValidatesWhenSubmitted $form) {
            $form->validate();
        });

        Asset::shortcuts([
            'wysiwyg'   => WysiwygShortcut::class,
        ]);

        $this->publishes([
            __DIR__ . '/Assets' => public_path('vendor/flowcontrol/form'),
        ], 'public');

        $this->loadViewsFrom(__DIR__ . '/Views', 'flowcontrol/form');
        $this->publishes([
            __DIR__ . '/Views' => resource_path('views/vendor/flowcontrol/form')
        ], 'views');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
           MakeFormCommand::class,
        ]);
    }
}
