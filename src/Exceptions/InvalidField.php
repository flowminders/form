<?php

namespace FlowControl\Form\Exceptions;

/**
 * @codeCoverageIgnore
 */
class InvalidField extends \Exception
{
}
