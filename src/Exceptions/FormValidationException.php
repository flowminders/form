<?php

namespace FlowControl\Form\Exceptions;

use Illuminate\Validation\ValidationException;

/**
 * @codeCoverageIgnore
 */
class FormValidationException extends ValidationException
{
}
